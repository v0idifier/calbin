all: build deploy

push:
	git push

build:
	rm -rf dist/
	pnpm run build
	bash -c 'gzip dist/*.{js,png,css,webmanifest} -9kf && brotli dist/*.{js,png,css,webmanifest} -Zkf'

deploy:
	ssh root@murciela.ga "rm -rf /srv/http/calbin.murciela.ga"
	rsync -rthvPu dist/ root@murciela.ga:/srv/http/calbin.murciela.ga/
	ssh root@murciela.ga "chown caddy:caddy -R /srv/http/calbin.murciela.ga"
