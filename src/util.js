import localForage from 'localforage'

export async function conseguirCalendarios () {
  return (await localForage.getItem('calendarios')) || {}
}

export async function crearCalendario (titulo) {
  const calendarios = await conseguirCalendarios()
  await localForage.setItem('calendarios', {
    ...calendarios, [titulo]: {},
  })
}
